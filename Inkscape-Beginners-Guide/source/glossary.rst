********
Glossary
********

.. glossary::

  Alpha
    Value for the opacity of a color, usually between 0 (fully transparent) and 1 (fully opaque) or between 0% and 100%. Alpha is tied to the color. Independently of this, each object can have an additional value for its opacity. The opacity value affects the object as a whole (as opposed to alpha, which affects only the transparency of the stroke or the fill of the object).

  AppImage
    A standalone packaging format for programs that run on Linux. AppImages are (mostly) self-contained executable files, that can be saved anywhere on your computer. To be able to run it as a program, you need to allow your computer to execute it by (usually) right-clicking on the file and editing its properties. This may mean to check a checkbox with the label 'file is executable'. More information on the format is availble at `appimage.org <https://appimage.org/>`__ .

  Binary Archive
    A folder that contains multiple binary files and requires a special type of program to interact with them.

  Binary File
    A type of file which is not meant to be read by humans. It contains no text but instead is composed of patterns of 0s and 1s that can be directly read by the computer.

  Bit
    (Short for binary digit) The smallest unit of data in a computer. A bit has a single binary value, either 0 or 1.

  Cap
    The shape that is put on the end nodes of a path. This can be a half circle or a half square, that is as wide as the path's stroke. Or the path can just be cut off at a 90° angle.

  Cascading Style Sheets
    A way to write out styling information for documents such as HTML and SVG. The CSS specification is developed by the W3C, just like the SVG file format. CSS determines how websites look in a browser. For SVG, it is one of two options to describe how an object is supposed to look.

  Clone
    A copy of another object that follows every change made to the original object. An object can have multiple clones that can all be positioned in different places in the drawing. Clones can be rotated and scaled independently of the original object. If the original's fill or stroke is not set (unset), the clone's fill (or stroke) can be edited. In all other cases, the clone's fill and stroke will look just like the original's.

  Executable File (.exe)
    Contains an executable program for Windows and is the standard file extension used by Windows programs. These are the files that make a program on your computer run.

  Flatpak
    A packaging format for Linux programs. Flatpak requires the Flatpak program for installation and running. It provides a sandbox to isolate the programs run as Flatpak from other programs on a user's system, to make running the program packaged as Flatpak safer for the user.

  Flow Text
    Text that automatically breaks lines after a given width.

  Glyph
    A graphical element that represents a specific character (letter, number, punctuation, ...) in a specific font.

  Hex Color
    A way to define a color by 3 numbers in the hexadecimal numeral system, commonly preceded by a hash (#) sign. Each number can take on values between 0 (no intensity) and 255 (maximum intensity), or 00 and ff, in hexadecimal. The first number stands for red, the second for green, and the last for blue. Combined, the colors represent one of the resulting 16,777,216 colors in the sRGB color space (commonly used for computer screens).

  HTML5
    The main language that websites are written in, in the (current) 5th edition. Like SVG, HTML is based on the XML markup structure, consisting of nested elements that are delimited by angle brackets. HTML is also standardized by the W3C.

  Join type
    The way that vector path corners look. They can be rounded (round), cut off (bevel) or pointed (miter).

  Ligature
    A combination of letters that has its own symbol in a font, mostly for cosmetic reasons. E.g. the letters s and t can be contracted to look like they are connected at the top, or the letters f and i are merged, so that the dot of the i becomes a part of the f's upper arc.

  Node
    A node is a point, placed at a specified position on a :term:`path <Path>`.

  Operating System Architecture
    A 32-bit architecture means that the operating system is able to process data chunks up to 32 bits in size. Likewise, a 64-bit architecture means an operating system can process data up to 64 bits. You need to use the correct program file for each architecture, otherwise the program won’t work properly.

    Note: Most modern computers come as 64-bit systems.

  Path
    The basic building element in SVG, consists of :term:`nodes <Node>` that are connected by the segments of the path.

  Portable Document Format
    A file format abbreviated as PDF which is used for exchanging and printing documents. Originally developed by Adobe in the 1990s, it has been standardized in 2008 and is now maintained by the International Organization for Standardization (ISO).

  Pixel
    The basic building element of a raster graphic, a dot of a specific color. Also, the smallest unit that creates a color on a computer screen, commonly consisting of three little lights, a red one, a green one and a blue one, which, when seen from a distance by the human eye, mix to look like a single color.

  PNG
    Abbreviation for 'Portable Network Graphics', a standardized raster graphics file format that supports transparency (unlike JPEG) and that uses lossless compression (unlike most JPEG images, no compression artifacts) and colors in the RGB color space.

  Portable App
    A portable app is a program that does not need to be installed onto a computer. Instead all necessary files needed to run the program reside in a single folder located on a disc or USB drive. The advantage to this is that the program can be taken anywhere the user goes and can be run from the cd or USB drive on any computer that supports the program. The disadvantages are that the program can easily be lost and if the USB isn’t properly removed from the computer after use, the program can get corrupted and stop working.

  Personal Package Archive
    A personal repository for Ubuntu packages hosted on `Launchpad <launchpad.net>`__ (abbreviated as 'ppa'). Ubuntu's package management tool 'apt' is able to load and install .deb packages published in a ppa. The Inkscape developers maintain a ppa for Inkscape.

  Scalable Vector Graphics (SVG)
    A file format for vector graphics that uses an open standard for saving the image data in a human-readable plain text format. The structure of the file follows the tree-like XML structure, similar to HTML (which is used for web pages). There are various computer programs that can display SVG files (almost all web browsers, for example), and many programs can save as SVG.

  Snap
    A packaging format for GNU/Linux programs, developed and promoted by Canonical, the makers of Ubuntu. It uses the package manager Snappy, and is available for multiple Linux distributions. Due to its strict default safety settings, the format comes with a couple restrictions for interacting with other resources and devices on a user's computer.

  Source code
    A set of text files that are written by software developers in one or more programming languages. These files contain the instructions for the computer program. In order for a computer to be able to execute the instructions, often the source code must be translated into binary code by a compiler program ('building' a program).

  Stroke
    A contour that is drawn along a vector path or shape. Strokes can have different widths, colors, patterns (dashes) and even gradients or patterns. Strokes are optional, not every path / shape has one.

  SVG Font
    A font whose file format is based on the SVG file format. Inkscape can create SVG fonts, but cannot use them for writing texts. Another program, like FontForge, is required to convert the SVG font to a different font file format (e.g. otf, ttf).

  Windows Installer Package Files (.msi)
    An MSI file is a package that contains installation information for a particular installer, such as files to be installed and the locations for installation. They can be used for Windows updates as well as third-party software installers. The installation results are no different from using an executable file, but msi packages sometimes do have additional options such as doing silent installations (no human action needed) or pre-configured installs.

  Zip File (.zip)
    Zip files are a way to compress large amounts of data into a smaller, more manageable size. This allows for easy transportation and faster downloads of files. The disadvantage to zip files is that the files inside need to be extracted and decompressed before they can be used.

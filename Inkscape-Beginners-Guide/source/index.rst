.. Inkscape Beginners' Guide documentation master file, created by
   sphinx-quickstart on Mon Oct 15 00:10:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Inkscape Beginners' Guide!
=========================================

Hi! Are you new to `Inkscape <https://inkscape.org>`__?

Learn more about Inkscape, an open source vector graphics editor, in this **free, community-created Beginners' Guide**:

.. image:: images/read_online.*
   :target: https://inkscape-manuals.readthedocs.io/en/latest/why-use-inkscape.html
   :alt: `Read Online`
   :class: third

.. image:: images/download_pdf.*
   :target: https://readthedocs.org/projects/inkscape-manuals/downloads/pdf/latest
   :alt: `Download PDF`
   :class: third

.. image:: images/download_epub.*
   :target: https://readthedocs.org/projects/inkscape-manuals/downloads/epub/latest/
   :alt: `Download EPUB`
   :class: third

This guide is a **living document**. This means that it is actively being edited and continuously being improved—**and you can contribute**!

If you find something that is confusing, wrong, or otherwise needs to be edited, `let us know <https://gitlab.com/inkscape/inkscape-docs/manuals/issues>`__!

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents
   :name: maintoc
   :includehidden:


.. toctree::
   :caption: Introduction
   :maxdepth: 1
   :hidden:

   why-use-inkscape
   professional-software

.. toctree::
   :caption: Installing Inkscape
   :maxdepth: 1
   :hidden:

   installing-on-windows
   installing-on-mac
   installing-on-linux


.. toctree::
   :caption: Finding Your Way Around
   :maxdepth: 1
   :hidden:

   interface
   managing-workspace


.. toctree::
   :caption: First Steps
   :maxdepth: 1
   :hidden:

   ways-drawing
   shape-tools
   selector-tool
   squares-and-rectangles
   circles-ellipses-and-arcs
   stars-and-polygons
   spirals
   3d-boxes
   stacking-order


.. toctree::
   :caption: Drawing Free Shapes
   :maxdepth: 1
   :hidden:

   free-drawing
   pencil-tool
   pen-tool
   calligraphy-tool
   boolean-operations
   editing-paths
   node-operations
   node-types
   objects-to-paths
   tweak-tool


.. toctree::
   :caption: Changing How Objects Look
   :maxdepth: 1
   :hidden:

   color
   palette
   fill-and-stroke-dialog
   custom-colors
   copying-a-color
   creating-gradients
   creating-a-mesh-gradient
   using-patterns
   modifying-patterns
   creating-custom-patterns
   strokes
   markers
   ordering-markers-stroke-and-fill
   custom-markers

.. toctree::
   :caption: Working with Text
   :maxdepth: 1
   :hidden:

   text
   writing-text
   styling-text
   changing-text-color
   moving-and-rotating-letters
   putting-text-on-path
   flowing-text-into-frame
   creating-custom-fonts

.. toctree::
   :caption: Save, Import and Export
   :maxdepth: 1
   :hidden:

   saving
   export-png
   export-pdf
   export-other-formats
   import-pictures
   import-other-formats

.. toctree::
   :caption: Useful Techniques
   :maxdepth: 1
   :hidden:

   techniques-overview
   copy-and-duplicate
   cloning-objects
   grouping
   align-and-distribute
   tracing-an-image
   clipping-and-masking
   filters
   extensions
   live-path-effects
   custom-keyboard-shortcuts

.. toctree::
   :caption: Beyond This Guide
   :maxdepth: 1
   :hidden:

   find-help
   draw-freely


.. toctree::
   :caption: Annex
   :maxdepth: 1
   :hidden:

   glossary
   about


.. toctree::
   :caption: For Editors of This Guide
   :maxdepth: 1
   :hidden:

   sample-chapter

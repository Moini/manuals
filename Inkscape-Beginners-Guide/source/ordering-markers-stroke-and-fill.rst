*********************************
Ordering Markers, Stroke and Fill
*********************************

An object's path determines its shape. The :term:`stroke <Stroke>` is centered on
the path, one half of it on the inside of the path, overlapping with the
fill, the other half is on the outside. Markers, too, are centered on the
path.

In the :guilabel:`Order` section of the :guilabel:`Fill and Stroke` dialog, you
can select the order in which these different parts of an object will be drawn.
This way, you can place markers (and / or strokes) above or below the fill.

|image0|

TODO: Example image needed!

[top] markers > stroke > fill [bottom]

|image1|

[top] markers > fill > stroke [bottom]

|image2|

[top] stroke > marker > fill [bottom]

|image3|

[top] stroke > fill > markers [bottom]

|image4|

[top] fill > markers > stroke [bottom]

|image5|

[top] stroke > fill > marker [bottom]

|image6|

.. |image0| image:: images/ordre-marqueurs.png
.. |image1| image:: images/order_markers_example_1.png
.. |image2| image:: images/order_markers_example_2.png
.. |image3| image:: images/order_markers_example_3.png
.. |image4| image:: images/order_markers_example_4.png
.. |image5| image:: images/order_markers_example_5.png
.. |image6| image:: images/order_markers_example_6.png
